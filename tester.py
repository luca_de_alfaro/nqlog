# Luca de Alfaro, 2020
# BSD License

"""THIS is how simple testing should be."""

import sys
import traceback

TESTS = []
num_success = 0
num_fail = 0
num_skipped = 0

def dont(func):
    """Decorator to skip a test."""
    def wrapper():
        global num_fail, num_success, num_skipped
        num_skipped += 1
    TESTS.append(wrapper)

def do(func):
    """Decorator to do a test."""
    global TESTS
    def wrapper():
        global num_fail, num_success, num_skipped
        try:
            print(".........", func.__name__, "start")
            func()
            print("-------->", func.__name__, "passed")
            num_success += 1
        except Exception:
            num_fail += 1
            print("--####-->", func.__name__, "FAILED:")
            traceback.print_exc(file=sys.stdout)
    TESTS.append(wrapper)

def do_all_tests():
    global num_fail, num_success, num_skipped
    for t in TESTS:
        t()
    print("Success:", num_success)
    print("Fail   :", num_fail)
    print("Skipped:", num_skipped)
