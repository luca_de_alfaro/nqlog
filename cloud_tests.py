import os
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

from nqlog import logthis, logging

from tester import do, dont, do_all_tests

@logthis()
def f():
    logging.log("first log line")
    logging.log("second log line")

@do
def test_twice():
    f()
    f()

do_all_tests()
