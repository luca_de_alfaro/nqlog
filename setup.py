
import setuptools

from nqlog  import __version__

setuptools.setup(
    name = "nqlog",
    version = __version__,
    url = 'https://bitbucket.org/luca_de_alfaro/nqlog/src/master/',
    license = 'BSD',
    author = 'Luca de Alfaro',
    author_email = 'luca@dealfaro.com',
    maintainer = 'Luca de Alfaro',
    maintainer_email = 'luca@dealfaro.com',
    description = 'Interface to Google Cloud Logging that enables aggregated operation logs',
    long_description = "",
    long_description_content_type = "text/markdown",
    packages = ['nqlog'],
    include_package_data = True,
    zip_safe = False,
    platforms = 'any',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
