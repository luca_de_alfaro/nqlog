
from nqlog import logthis, logging
from tester import do, dont, do_all_tests

import os
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"


@logthis()
def f():
    print("--a--")
    logging.log("first log line")
    print("--b--")
    logging.log("second log line")
    print("--c--")

@logthis(level="INFO", max_duration=1)
def g():
    import time
    logging.debug("This is a debug line")
    logging.info("This is an info line")
    time.sleep(2)
    logging.info("This is a %s with %r", "line", 3.45)
    logging.warning("This is a warning line")

@logthis(local=True)
def h():
    print("--a--")
    logging.log("first log line")
    print("--b--")
    logging.log("second log line")
    print("--c--")

@do
def test_local():
    h()

@do
def test_twice():
    f()
    f()

@do
def test_levels():
    f()
    g()


do_all_tests()
